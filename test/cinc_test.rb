# frozen_string_literal: true

require 'json'

require 'test_helper'

class CincTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::Cinc::VERSION
  end

  def test_it_spawns_a_vehicle
    position = Cinc::DCS::Model::Position.new(lat: 44.6, lon: 39.5)


    group = Cinc::DCS::Model::Group.new(position)
    group.name = "testSpawnsVehicle"


    group.route[:points] << {
      name: "Waypoint 1",
      type: "Turning Point",
      x: 45,
      y: 40,
      speed: 50
    }

   group.uncontrollable = false




    unit = Cinc::DCS::Model::Unit.new(name: "tanks", type: "M-1 Abrams", skill_level: :average, position: nil)  
    unit.name = "testSpawnsVehicle1"
    unit.position = position

    group.units = [unit]

    criteria = Cinc::DCS::API::AddGroup::Criteria.new(
      country_id: 2,
      group_category: 2,
      group: group
    )

    result = JSON.parse(Cinc::DCS::API::AddGroup.call(criteria))

    puts result

    assert("testSpawnsVehicle", result['group_name'])

  end
end
