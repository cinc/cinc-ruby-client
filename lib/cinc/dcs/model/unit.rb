# frozen_string_literal: true

require 'json'

module Cinc
  module DCS
    module Model
      class Unit
        SKILL_LEVEL = {
          random: 'Random',
          average: 'Average',
          good: 'Good',
          high: 'High',
          excellent: 'Excellent'
        }.freeze

        attr_accessor :name, :type, :unit_id, :position,
                      :player_can_drive, :transportable

        attr_reader :skill_level

        def initialize(name:, type:, skill_level:, position:)
          self.name = name
          self.type = type
          self.skill_level = skill_level
          self.position = position
        end

        def skill_level=(skill_level)
          raise ArgumentError unless SKILL_LEVEL.key?(skill_level)
          @skill_level = skill_level
        end

        def to_json(_stuff = nil)
          {
            type: type,
            unitId: unit_id,
            skill: SKILL_LEVEL[skill_level],
            playerCanDrive: player_can_drive,
            name: name,
            x: position.lat,
            y: position.lon,
            heading: 0,
            transportable: { randomTransportable: transportable }
          }.to_json
        end
      end
    end
  end
end
