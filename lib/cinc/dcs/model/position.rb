# frozen_string_literal: true

module Cinc
  module DCS
    module Model
      class Position
        attr_accessor :lat, :lon, :altitude

        def initialize(lat:, lon:, altitude: nil)
          self.lat = lat
          self.lon = lon
          self.altitude = altitude
        end

        def to_json
          {
            x: lat,
            y: lon,
            altitude: altitude,
          }
        end
      end
    end
  end
end
